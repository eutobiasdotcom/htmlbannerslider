﻿// JavaScript Document
(function($){  
	$.fn.extend({   
		tbSlider: function(options) {  
			return this.each( function() {  
				
				var opt = options || { next : 'NEXT' , prev : 'PREV', position : 'horizontal', thumbs : false, timeout : 2000, autoChange : false }
				
				var sliderMeasure = 0;
				var container = $(this);
				var slider = $('<div class="slider"></div>');
				var sliderBox = $('<div class="sliderBox"></div>');
				var itens = container.find('div.item');	
				var thumbsContainer;
				var timeoutInterval;
				
				itens.each (function () {
					sliderMeasure += ( opt.position == 'vertical') ? $(this).height() : $(this).width();
				});
				
				var stepMeasure = ( opt.position == 'vertical' ) ? (itens.eq('0').css('height')).replace('px','') : (itens.eq('0').css('width')).replace('px','');
				var moveCount = 0;
				
				//FUNCTIONS
				
				var init = function () {					
					slider.append(itens);					
					sliderBox.append(slider);					
					container.append(sliderBox);
					
					if ( opt.position == 'vertical' )
						slider.height(sliderMeasure);
					else 
						slider.width(sliderMeasure);
						
					createControllers();
					
					if (opt.thumbs)
					{
						createThumbs();
						selectThumb(0);
					}
					
					if (opt.autoChange)
					{
						timer();
					}
					
				};
				
				var next = function () {
					clearInterval(timeoutInterval);
					
					if ( ++moveCount >= itens.length )
					{
						moveCount = 0; 
					}					
					selectThumb(moveCount);
					animate();
					
					timer();
				}
				
				var prev = function () {
					clearInterval(timeoutInterval);
					
					if ( --moveCount < 0 )
					{
						moveCount = (itens.length - 1); 
					}
					selectThumb(moveCount);
					animate();
					
					timer();
				}
				
				var seek = function(index) {
					clearInterval(timeoutInterval);
					
					moveCount = index;
					animate();
					
					timer();
				}
				
				var animate = function () {
					var pos = -( stepMeasure * moveCount );
					
					if (opt.position == 'vertical' )
						slider.animate( { marginTop : pos, duration : 1000 } );
					else
						slider.animate( { marginLeft : pos, duration : 1000 } );
				}
				
				var createControllers = function () {
					var lftArrow = $('<div class="nextArrow" style="position:absolute;z-index:5;cursor:pointer;">' + opt.next + '</div>');
					var rgtArrow = $('<div class="prevArrow" style="position:absolute;z-index:5;cursor:pointer;">' + opt.prev + '</div>');
					
					container.append(lftArrow);
					container.append(rgtArrow);
					
					lftArrow.click(function () {
						next();
						//animate();
					});				
					
					rgtArrow.click(function () {
						prev();
						//animate();
					});					
				}
				
				var createThumbs = function () {
					thumbsContainer = $('<div class="thumbsContainer"></div>');
					
					var itemIndex = 0;
					itens.each(function () {
						var img = $(this).find('img').clone();
						
						img.addClass('thumb');
						img.click( function () { 
							var index = $(this).index();
							seek(index); 
							selectThumb(index);
						});						
						thumbsContainer.append(img);						
					});					
					container.append(thumbsContainer);
				}
				
				var selectThumb = function (index) {
					if (opt.thumbs)
					{
						thumbsContainer.find('img.thumb').each (function () { $(this).removeClass('selected'); });
						thumbsContainer.find('img.thumb').eq(index).addClass('selected');
					}
				}
				
				var timer = function () {
					if (opt.autoChange) {
						timeoutInterval = setTimeout(next , opt.timeout);
					}
				}	
				
				init();
				
			});			
		}  
	});  
})(jQuery); 